all: download-htmx
	npx tailwindcss -i websrc/index.css -o dist/index.css
	cp websrc/index.html dist/index.html
	go build

dist/htmx.min.js:
	wget https://unpkg.com/htmx.org@1.9.8/dist/htmx.min.js -O dist/htmx.min.js

download-htmx: dist/htmx.min.js


.PHONY: clean
clean:
	rm -rf ./dist
	mkdir -p ./dist

