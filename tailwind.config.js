/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./websrc/**/*.{html,js}"],
  theme: {
    extend: {
      gridTemplateColumns: {
        '2': 'repeat(2, minmax(0, 0.25fr))',
      }
    },
  },
  plugins: [],
}

