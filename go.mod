module github.com/irishpatrick/htmx-go

go 1.21.4

require github.com/gorilla/mux v1.8.1

require github.com/chasefleming/elem-go v0.14.0
