package main

import (
	"embed"
	"fmt"
	"io/fs"
	"net/http"

	"github.com/chasefleming/elem-go"
	"github.com/chasefleming/elem-go/attrs"
	"github.com/gorilla/mux"
)

//go:embed dist/*
var staticFS embed.FS

func getStaticFS() http.FileSystem {
	fs, err := fs.Sub(staticFS, "dist")
	if err != nil {
		panic(err)
	}
	return http.FS(fs)
}

var balance int = 0

func main() {
	router := mux.NewRouter()

	// endpoints
	router.HandleFunc("/spend", SpendHandler).Methods("POST")
	router.HandleFunc("/earn", Earnhandler).Methods("POST")
	router.HandleFunc("/balance", BalanceHandler).Methods("GET")

	// static files
	router.PathPrefix("/").Handler(http.FileServer(getStaticFS()))

	err := http.ListenAndServe(":8000", router)
	if err != nil {
		panic(err)
	}
}

func SpendHandler(w http.ResponseWriter, r *http.Request) {
	balance -= 15
	BalanceHandler(w, r)
}

func Earnhandler(w http.ResponseWriter, r *http.Request) {
	balance += 15
	BalanceHandler(w, r)
}

func BalanceHandler(w http.ResponseWriter, r *http.Request) {
	var class string
	if balance < 0 {
		class = "p-red"
	} else {
		class = "p-black"
	}
	hours := int(balance / 60)
	minutes := balance % 60
	if hours < 0 {
		minutes *= -1
	}

	content := elem.P(
		attrs.Props{
			attrs.ID:    "balance",
			attrs.Class: class,
		},
		elem.Text(fmt.Sprintf("%dh %dm", hours, minutes)),
	)
	w.Write([]byte(content.Render()))
}
